import React, { Component } from 'react';
import {Navbar, NavItem, Icon, Row, Col} from 'react-materialize';
import 'materialize-css'
import './index.css'

import Menur from './sideNavi.js'




class Header extends Component {
	render() {
		return ( 
			<Navbar>
				<Row>
					<Col m={6}>
						<Menur />
					</Col>
					<Col m={6} className="search-wrap">
						<NavItem  href='#'><Icon>search</Icon></NavItem>
					</Col>
				</Row>
			</Navbar>
		)
	}
}
export default Header